<?php

/**
 * @file
 * Generates HTML output for the PGN field.
 */

// index of main-variantion
define ('MAIN', 0);

/*
 * Create a new game structure.
 */
function pgn2html_new_game() {
  return array(
    'event' => '',
    'site' => '',
    'date' => '',
    'round' => '',
    'white' => '',
    'black' => '',
    'result' => '',
    'whiteelo' => '',
    'blackelo' => '',
    'annotator' => '',
    'plycount' => '',
    'timecontrol' => '',
    'time' => '',
    'eventdate' => '',
    'setup' => '',
    'eventtype' => '',
    'eventrounds' => '',
    'eventcountry' => '',
    'source' => '',
    'sourcedate' => '',
    'whiteteam' => '',
    'blackteam' => '',
    'moves' => '',
    'move_size' => 0,
    'notation' => '',
    'initial' => '',
    'variants' => array(),
  );
};

/*
 * Define new position structure.
 */
function pgn2html_new_position() {
  return array(
        'board' => array_fill(0, 8, array_fill(0, 8, 0)),
        'turn' => 0,
        'wkcr' => FALSE,          /* castling rights */
        'wqcr' => FALSE, 
        'bkcr' => FALSE, 
        'bqcr' => FALSE,            
        'ep_col' => 0,            /* en passant column */
        'reversable_moves' => 0,  /* for 50-move rule */
  );
};

/*
 * Define a new variation.
 */
function pgn2html_new_variation() {
  return array(
      'buffer' => '',
      'id' => 0,
      'parent_move' => 0,
      'actual_move' => 0,
      'parent' => -1,
      'siblings' => -1,
      'children' => -1,
      'relative_move' => 0,
      'FEN' => '',
      'position' => 0,
      'previous_position' => 0,
      'reversable_moves' => 0, /* for 50-move rule */
  );
};

/*
 * Define new move structure.
 */
function pgn2html_new_move() {
  return array(
      'from_col' => 0, 
      'from_row' => 0, 
      'to_col' => 0, 
      'to_row' => 0,
      'promotion_piece' => 0,
  );
};

/* 
 * process pgn game 
 */
function pgn2html_process_game($path, $pieces) {
  $game = pgn2html_new_game();

  $handle = fopen($path, 'r');
    
  //TODO: Check if more then 1 PGN is in file.
  
  /* read the file line by line */
  while (($read_line = fgets($handle, 256)) !== FALSE) {
    // check if one of the items is in the file_content
    sscanf($read_line, "[Event \"%[^\"]\"]", $game['event']);
    sscanf($read_line, "[Site \"%[^\"]\"]", $game['site']);
    sscanf($read_line, "[Date \"%[^\"]\"]", $game['date']);
    sscanf($read_line, "[Round \"%[^\"]\"]", $game['round']);
    sscanf($read_line, "[White \"%[^\"]\"]", $game['white']);
    sscanf($read_line, "[Black \"%[^\"]\"]", $game['black']);
    sscanf($read_line, "[Result \"%[^\"]\"]", $game['result']);
    sscanf($read_line, "[FEN \"%[^\"]\"]", $game['FEN']);
    sscanf($read_line, "[Annotator \"%[^\"]\"]", $game['annotator']);
    sscanf($read_line, "[TimeControl \"%[^\"]\"]", $game['timecontrol']);
    sscanf($read_line, "[Time \"%[^\"]\"]", $game['time']);
    sscanf($read_line, "[BlackElo \"%[^\"]\"]", $game['blackelo']);
    sscanf($read_line, "[WhiteElo \"%[^\"]\"]", $game['whiteelo']);
    sscanf($read_line, "[PlyCount \"%[^\"]\"]", $game['plycount']);
    sscanf($read_line, "[EventDate \"%[^\"]\"]", $game['eventdate']);
    sscanf($read_line, "[Setup \"%[^\"]\"]", $game['setup']);
    sscanf($read_line, "[EventType \"%[^\"]\"]", $game['eventtype']);
    sscanf($read_line, "[EventRounds \"%[^\"]\"]", $game['eventrounds']);
    sscanf($read_line, "[EventCountry \"%[^\"]\"]", $game['eventcountry']);
    sscanf($read_line, "[Source \"%[^\"]\"]", $game['wource']);
    sscanf($read_line, "[SourceDate \"%[^\"]\"]", $game['sourcedate']);
    sscanf($read_line, "[WhiteTeam \"%[^\"]\"]", $game['whiteteam']);
    sscanf($read_line, "[BlackTeam \"%[^\"]\"]", $game['blackteam']);
    
    if (!strcmp("\n", $read_line)) { 
      break; 
    }
    if (!strcmp("\r\n", $read_line)) { 
      break; 
    }
  }

  pgn2html_process_moves($handle, $game);
  pgn2html_print_initial_position($game, "initial");
  
  $html = pgn2html_process_html($game, $pieces);

  fclose($handle);
  
  return $html;
}

/*
 * Creates a HTML output for the game data.
 */
function pgn2html_process_html($game, $diagram_pieces) {  
  $html = "";
  $html .= "<script type=\"text/javascript\">\n";
  $html .= "// <![CDATA[\n";
  // define directory where png files can be found
  $html .= "piecesDirectory = \"" . url("", array('absolute' => TRUE)) . drupal_get_path('module', 'pgn2html') . "/pieces/" . $diagram_pieces . "\";\n";  
  ////Start position, moves and variations
  $html .= $game['initial'] . "\n";
  $html .= $game['moves'] . "\n";
  $html .= "// ]]>\n";
  $html .= "</script>\n";

  $html .= "<div class=\"pgn2html\">\n";
  $html .= "  <div id=\"leftside\">\n";
  $html .= "    <div id=\"insideleft\">\n";
  $html .= "      <div id=\"diagram\"></div>\n";
  $html .= "      <div id=\"controls\"></div>\n";
  $html .= "    </div>\n";
  $html .= "  </div>\n";
 
  $html .= "  <div id=\"rightside\">";
  $html .= "    <div id=\"insideright\">\n";
  $html .= "      <h2>" . $game['white'] . " - " . $game['black'] . "</h2>\n";
  $html .= "      <h3>" . $game['round'] . " " . $game['event'] . " " . $game['site'] . " " . $game['date'] . "</h3>\n";
  $html .= "      <div class=\"game\">" . $game['notation'] . "</div>\n";
  $html .= "      <div class=\"result\">" . $game['result'] . "</div>\n";
  $html .= "    </div>\n";
  $html .= "  </div>\n";
  $html .= "</div>\n";
  
  return $html;
}

/* 
 * create html & javascript data for moves in pgn file. 
 */
function pgn2html_process_moves($handle, &$game) {
  $new_variant = MAIN;
  $token = '';
  $temp = '';
  $move_string = '';
  $nag = 0;
  $token_pos = 0;
  $in_comment = FALSE;
  $left_comment = FALSE;
  $left_variation = FALSE;
  $entered_variation = FALSE;
  $token_len = 0;
  $notation = '';
  
  $move = pgn2html_new_move();
  
  // Create main variant.
  $game['variants'][$new_variant] = pgn2html_new_variation();
  $game['variants'][$new_variant]['position'] = pgn2html_new_position();
  $game['variants'][$new_variant]['id'] = $new_variant;
  $game['variants'][$new_variant]['buffer'] = "moves[" . $game['variants'][$new_variant]['id'] . "] = new Array(";
  $game['variants'][$new_variant]['parent_move'] = 0;
  $game['variants'][$new_variant]['actual_move'] = 1;
  $game['variants'][$new_variant]['relative_move'] = 1;
  
  pgn2html_setup_board($game, $new_variant);
  
  // Set pointer to main variant
  $current = $new_variant;
  
  $notation .= "<span class=\"mainline\">";
  $process_game = TRUE;

  /* parse move text */
  while ($process_game) {   
    /* fetch next token */
    if (fscanf($handle, "%[ !-}]", $token) != 1) {
      $game['variants'][$current]['buffer'] = "-1,-1,-1,-1);\n"; /* exit loop if none */
      $notation .= "</span>";
      
      break;
    }
    
    while (strcmp($token, '')) {    
      if ($in_comment) {
        if (strchr($token, '}')) {
           // search for end of comment
          $token_pos = strpos($token, '}');
          
          // add $token with new end of string to notation
          $notation .= " ";
          $notation .= substr($token, 0, $token_pos);
          $notation .= " </span>";

          // Skip the }
          $token_pos++;
          
          // copy rest of token to temp and then to token
          //TODO: Check on empty string
          $temp =  substr($token, $token_pos);
          $token = $temp;
          
          $in_comment = FALSE;
          $left_comment = TRUE;
          continue;
        }
        else {
          $notation .= " ";
          $notation .= $token;
          $token = "";
          continue;
        }
      }
           
      /* Start of a comment? */
      if ($token[0] == '{') {
        if ($current['id'] == MAIN) {
          $notation .= "</span> <span class=\"comment\">";
        }
                
        $temp =  substr($token, 1);
        $token = $temp;
        $in_comment = TRUE;
        
        continue;
      }

      /* check for result */
      if (!strcmp($token, '1-0') || strcmp($token, '0-1') == 0 || !strcmp($token, '1/2-1/2') || !strcmp($token, '*')) {
        $game['variants'][$current]['buffer'] .= "-1,-1,-1,-1);\n";
        $process_game = FALSE;
        break;
      }
           
      /* Replace NAGS with comment/symbol */
      if ($token[0] == '$') {
        sscanf(substr($token, 1), "%d", $nag);

        if ($nag < 140) {
          
          $nag_str = pgn2html_nag($nag);
          if (ctype_alpha($nag_str[0])) {
            $notation .= " ";
          }
          
          $notation .= "<span class=\"nag\">" . $nag_str . "</span>";;
        }

        $token_pos++;
        
        while (is_numeric($token[$token_pos])) {
          $token_pos++;
        }
        
        $temp = substr($token, $token_pos);
        $token = $temp;
        continue;
      }

      if ($token[0] == ' ') {
        // We are not in comment and after nag, so every space can be removed.
        $temp =  substr($token, 1);
        $token = $temp;
        continue;
      }
 
      /* Start of a variation? */
      if ($token[0] == '(') {      
        $temp = substr($token, 1);
        $token = $temp;
        
        if ($current == MAIN) {
          $notation .= "</span>";
        }
        
        if (!$entered_variation) {
          $notation .= " ";
        }
        
        $notation .= "<span class=\"variant\">(";

        /* create child variation */
        $new_variant++;
        $game['variants'][$new_variant] = pgn2html_new_variation();
        $game['variants'][$new_variant]['parent'] = $game['variants'][$current]['id'];
        
        $game['variants'][$new_variant]['parent_move'] = $game['variants'][$current]['relative_move'] - 2;
        $game['variants'][$new_variant]['actual_move'] = $game['variants'][$current]['actual_move'] - 1;
        $game['variants'][$new_variant]['relative_move'] = 1;
        $game['variants'][$new_variant]['position'] = $game['variants'][$current]['previous_position'];
        $game['variants'][$new_variant]['id'] = $new_variant;
        $game['variants'][$new_variant]['buffer'] = "moves[" . $new_variant . "] = new Array(";

        /* add variation to tree */
        if ($game['variants'][$current]['children'] > -1) {
               
          while ($game['variants'][$current]['siblings'] > -1) {
            $current = $game['variants'][$current]['siblings'];
          }      
          
          $game['variants'][$current]['siblings'] = $new_variant;
        }
        else {
          $game['variants'][$current]['children'] = $new_variant;
        }

        /* make the new one the current variation */
        $current = $new_variant;
        $entered_variation = TRUE;
        $left_comment = FALSE;
        $left_variation = FALSE;
        continue;
      }

      /* end of variation? */
      if ($token[0] == ')') {
        $temp = substr($token, 1);
        $token = $temp;
        $notation .= ")</span>";

        /* terminate variation */
        if ($game['variants'][$current]['parent'] > -1) {
          $game['variants'][$current]['buffer'] .= "-1,-1,-1,-1);\n";
          
          // set current to the parent line.
          $current = $game['variants'][$current]['parent'];
        }
        
        $entered_variation = FALSE;
        $left_variation = TRUE;
        continue;
      }

      /* move number? */
      if (is_numeric($token[0]) || $token[0] == '.') {
        $temp =  substr($token, 1);
        $token = $temp;
        continue;
      }

      /* parse move */
      $temp = $token;
      $token_pos = 0;
      $token_len = strlen($token);
      
      while (($token_pos < $token_len) && ((ctype_alnum($token[$token_pos]) || $token[$token_pos] == '+' || $token[$token_pos] == '-' || $token[$token_pos] == '#' || $token[$token_pos] == '='))) {
        $token_pos++;
      }

      /* if no valid characters, we are confused so abort */
      if ($token_pos == 0) {
        $token = '';
        continue;
      }
      
      $token = substr($token, $token_pos);
      $move_string = substr($temp, 0, $token_pos);
      $token_pos = 0;
  
      /* convert the move, checking for failure */
      pgn2html_algebraic_to_move($move, $move_string, $game['variants'][$current]['position']);  
      if ($move['from_col'] == -1) {
        continue;
      }
 
      if ($current == MAIN && ($left_comment || $left_variation)) {
        // Start to notate main line in bold.
        $notation .= "<span class=\"mainline\">";
      }
      
      if (!$entered_variation)
        $notation .= " ";

      if ($game['variants'][$current]['position']['turn'] == WHITE) {
        $notation .= (int)(($game['variants'][$current]['actual_move'] + 1) / 2) . ".";
      }
      else {
        if ($game['variants'][$current]['relative_move'] == 1 || $left_comment || $left_variation) {
          $notation .=(int)(($game['variants'][$current]['actual_move'] + 1) / 2) . ". ...";
        }
      } 

      $notation .= " <a class=\"move\" href=\"javascript:jumpto(" . $current . ", " . $game['variants'][$current]['relative_move'] . ");\" id=\"v" . $current . "m" . $game['variants'][$current]['relative_move'] . "\">" . $move_string . "</a>\n";
      $game['variants'][$current]['buffer'] .= pgn2html_append_move($move, $game['variants'][$current]['position']);
      
      /* execute move */
      $game['variants'][$current]['previous_position'] = $game['variants'][$current]['position'];
      pgn2html_make_move($game['variants'][$current]['position'], $move);
      $left_comment = FALSE;
      $left_variation = FALSE;
      $entered_variation = FALSE;
      $game['variants'][$current]['actual_move']++;
      $game['variants'][$current]['relative_move']++;    
    }
  }
  
  $game['notation'] = $notation;
  
  pgn2html_setup_board($game, MAIN);
    
  /* Merging buffers into moves buffer */
  if ($game['variants'][MAIN]['actual_move'] > 1)
    pgn2html_delete_variation($game['variants'], MAIN, $game['moves']);
}

/* append to string move as javascript data */
function pgn2html_append_move($move, $position) {
  /* special moves must be broken down into 2 moves for simple javascript code e.g. castling requires moving two pieces */

  $js_move = array(-1, -1, -1, -1);
  $js_move[0] = $move['from_col'] + 8 * (7 - $move['from_row']);
  $js_move[1] = $move['to_col'] + 8 * (7 - $move['to_row']);

  /* check for special pawn moves */
  if (pgn2html_piece_to_piece_type($position['board'][$move['from_col']][$move['from_row']]) == PAWN) {
    /* check if move is a promotion */
    if ($move['promotion_piece']) {
      $js_move[2] = -(int)pgn2html_piece_type_and_colour_to_piece($move['promotion_piece'], $position['turn']);
    }
    else {
      /* check if move is an en passant capture */
      if ($move['to_col'] == $position['ep_col'] && $move['to_row'] == ($position['turn'] == WHITE ? 5 : 2)) {
        $js_move[2] = $js_move[0];
        $js_move[3] = $js_move[1];
        $js_move[0] = $move['to_col'] + 8 * (7 - $move['from_row']);
        $js_move[1] = $move['to_col'] + 8 * (7 - $move['to_row']);
      }
    }
  }
  else {
    /* check if move is a castling move */
    if (pgn2html_piece_to_piece_type($position['board'][$move['from_col']][$move['from_row']]) == KING) {       
      /* kingside? */
      if ($move['to_col'] - $move['from_col'] == 2) {
        $js_move[2] = 7 + 8 * (7 - $move['from_row']);
        $js_move[3] = $js_move[2] - 2;
      }

      /* queenside? */
      if (($move['from_col'] - $move['to_col']) == 2) {
        $js_move[2] = 0 + 8 * (7 - $move['from_row']);
        $js_move[3] = $js_move[2] + 3;
      }
    }
  }
  
  /* now write javascript move to string */
  return $js_move[0] . ',' . $js_move[1] . ',' . $js_move[2] . ',' . $js_move[3] . ',';
}

/* output javascript data for initial position */
function pgn2html_print_initial_position(&$game, $var) {
  $html = '';
  $position = $game['variants'][MAIN]['position'];
  
  /* Print out position */
  $html = "var " . $var . " = new Array(";

  for ($row = 7; $row >= 0; $row--) {
    for ($col = 0; $col < 8; $col++) {
      if ($col == 7 && $row == 0) {
        $html .= $position['board'][$col][$row] . ");\n";
      }
      else {
        $html .= $position['board'][$col][$row] . ",";
      }
    }
  }
  
  $game['initial'] = $html;
}

/* deletes a variation adding its data to the moves string */
function pgn2html_delete_variation($variations, $var, &$moves) {
  /* add parent information to moves */
  $moves .= "parents[" . $variations[$var]['id'] . "] = new Array(";
    
  if ($variations[$var]['parent'] > -1) {
    $moves .= $variations[$var]['parent'] . "," . $variations[$var]['parent_move'] . ");\n"; 
  }
  else {
    $moves .= "-1," . $variations[$var]['parent_move'] . ");\n"; 
  }
 
  /* add buffer to moves */
  $moves .= $variations[$var]['buffer'];

  /* if there are children proces these first */
  if ($variations[$var]['children'] > -1) {
    pgn2html_delete_variation($variations, $variations[$var]['children'], $moves);
  }

  /* if there are siblings proces these second */
  if ($variations[$var]['siblings'] > -1) {
    pgn2html_delete_variation($variations, $variations[$var]['siblings'], $moves);
  }
}
