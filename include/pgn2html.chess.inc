<?php

/**
 * @file
 * Defines hooks for the pgn2html module.
 */

define('INITIAL_POSITION', "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");

define ('NO_PIECE', 0);
define ('WPAWN', 1);
define ('WKNIGHT', 2);
define ('WBISHOP', 3);
define ('WROOK', 4);
define ('WQUEEN', 5);
define ('WKING', 6);
define ('BPAWN', 7);
define ('BKNIGHT', 8); 
define ('BBISHOP', 9);
define ('BROOK', 10);
define ('BQUEEN', 11);
define ('BKING', 12);

define ('NO_COLOUR', 0);
define ('WHITE', 1);
define ('BLACK', 2);

define ('NO_PIECE_TYPE', 0); 
define ('PAWN', 1);
define ('KNIGHT', 2); 
define ('BISHOP', 3);
define ('ROOK', 4);
define ('QUEEN', 5);
define ('KING', 6); 

define ('NO_RESULT', 0);  
define ('DRAW', 1); 
define ('WHITE_WIN', 2); 
define ('BLACK_WIN', 3); 

define ('NO_RESULT_TYPE', 0);
define ('CHECKMATE', 1);
define ('STALEMATE', 2);
define ('THREE_FOLD_REPETITION', 3);
define ('FIFTY_MOVE_RULE', 4);
define ('INSUFFICIENT_MATERIAL', 5); 

/*
 * Create a null move structure.
 */
function pgn2html_null_move() {
  return array(
    'from_col' => -1,
    'from_row' => -1,
    'to_col' => -1,
    'to_row' => -1, 
    'promotion_piece' => NO_PIECE_TYPE,
  );
};

/*
 * Define the white kingside castling move.
 */
function pgn2html_wcastlekingside() {
    return array(
    'from_col' => 4,
    'from_row' => 0,
    'to_col' => 6,
    'to_row' => 0, 
    'promotion_piece' => NO_PIECE_TYPE,
  );
};

/*
 * Define the white queenside castling move.
 */
function pgn2html_wcastlequeenside() {
    return array(
    'from_col' => 4,
    'from_row' => 0,
    'to_col' => 2,
    'to_row' => 0, 
    'promotion_piece' => NO_PIECE_TYPE,
  );
};

/*
 * Define the black kingside castling move.
 */
function pgn2html_bcastlekingside() {
    return array(
    'from_col' => 4,
    'from_row' => 7,
    'to_col' => 6,
    'to_row' => 7, 
    'promotion_piece' => NO_PIECE_TYPE,
  );
};

/*
 * Define the black queenside castling move.
 */
function pgn2html_bcastlequeenside() {
    return array(
    'from_col' => 4,
    'from_row' => 7,
    'to_col' => 2,
    'to_row' => 7, 
    'promotion_piece' => NO_PIECE_TYPE,
  );
};

/*
 * Returns the allowed moves of a piece.
 */
function pgn2html_move_vector(&$move_vector, $piece, $vector) {
  $vectors = array( array( array(1, 2, 1), array(2, 1, 1), array(2, -1, 1), array(1, -2, 1), array(-1, -2, 1), array(-2, -1, 1), array(-2, 1, 1), array(-1, 2, 1)),
                    array( array(1, 1, 7), array(1, -1, 7), array(-1, -1, 7), array(-1, 1, 7), array(0, 0, 0), array(0, 0, 0), array(0, 0, 0), array(0, 0, 0)),
                    array( array(0, 1, 7), array(1, 0, 7), array(0, -1, 7), array(-1, 0, 7), array(0, 0, 0), array(0, 0, 0), array(0, 0, 0), array(0, 0, 0)),
                    array( array(0, 1, 7), array(1, 0, 7), array(0, -1, 7), array(-1, 0, 7), array(1, 1, 7), array(1, -1, 7), array(-1, -1, 7), array(-1, 1, 7)),
                    array( array(0, 1, 1), array(1, 0, 1), array(0, -1, 1), array(-1, 0, 1), array(1, 1, 1), array(1, -1, 1), array(-1, -1, 1), array(-1, 1, 1))
             );
  
  $move_vector['col_vector'] = $vectors[$piece - 2][$vector][0];
  $move_vector['row_vector'] = $vectors[$piece - 2][$vector][1];
  $move_vector['range'] = $vectors[$piece - 2][$vector][2];  
};

/*
 * Define a new move vector.
 */
function pgn2html_new_move_vector() {
  return array(
      'col_vector' => 0,
      'row_vector' => 0,
      'range' => 0,
  );
};

/*
 * Define a new move node.
 */
function pgn2html_new_move_node() {
  return array(
    'id' => -1,
    'previous' => 0,
    'move' => array(),
    'next' => 0,
  );
};

/*
 * Adds a move to the move array.
 */
function pgn2html_add_move_to_list(&$move_list, $from_col, $from_row, $to_col, $to_row) {
  $node = pgn2html_new_move_node();
  $id = array_push($move_list, $node);
   
  // array is zerobased, so lower the id.
  $id--;
  
  $move_list[$id]['id'] = $id;
  $move_list[$id]['move'] = pgn2html_new_move();
  $move_list[$id]['move']['from_col'] = $from_col;
  $move_list[$id]['move']['from_row'] = $from_row;
  $move_list[$id]['move']['to_col'] = $to_col;
  $move_list[$id]['move']['to_row'] = $to_row;
  $move_list[$id]['move']['promotion_piece'] = NO_PIECE_TYPE;

  /* add node to list */
  $move_list[$id]['next'] = -1;
  
  // Check if we have already moves.
  if ($id > 0) {
    $move_list[$id -1]['next'] = $id;
    $move_list[$id]['previous'] = $move_list[$id-1]['id'];
  }
  else {
    $move_list[$id]['previous'] = -1;
  }
};

function pgn2html_add_promotion_to_list(&$move_list, $from_col, $from_row, $to_col, $to_row) {
  // For each promotion piece we must create a move.
  for ($promotion_piece = KNIGHT; $promotion_piece <= QUEEN; $promotion_piece++) {
    $node = pgn2html_new_move_node();
    $id = array_push($move_list, $node);
   
    // array is zerobased, so lower the id.
    $id--;
  
    $move_list[$id]['id'] = $id;
    $move_list[$id]['move'] = pgn2html_new_move();
    $move_list[$id]['move']['from_col'] = $from_col;
    $move_list[$id]['move']['from_row'] = $from_row;
    $move_list[$id]['move']['to_col'] = $to_col;
    $move_list[$id]['move']['to_row'] = $to_row;
    $move_list[$id]['move']['promotion_piece'] = $promotion_piece;
      
   /* add node to list */
    $move_list[$id]['next'] = -1;
  
    // Check if we have already moves.
    if ($id > 0) {
      $move_list[$id -1]['next'] = $id;
      $move_list[$id]['previous'] = $move_list[$id-1]['id'];
    }
    else {
      $move_list[$id]['previous'] = -1;
    }
  }
};

function pgn2html_setup_board(&$game, $var_id) {
  $col = 0;
  $row = 7;
  $fen_ptr = 0;
  
  /* decide on start position */
  if (empty($game['FEN'])) {
    $game['FEN'] = INITIAL_POSITION;
  }
 
  $game_fen = &$game['FEN'];
  
  $root = &$game['variants'][$var_id];

  /* parse placement data */
  while ($row >= 0) {
    $char = $game_fen[$fen_ptr];
    
    if ($char == '/') {
      $col = 0;
      $row--;
      $fen_ptr++;
      continue;
    }

    if (ctype_alpha($char)) {
      $root['position']['board'][$col][$row] = pgn2html_char_to_piece($char);
      $col++;
      $fen_ptr++;
    }
    else {
      for ($fill = 0; $fill < (ord($char) - ord('0')); $fill++) {
        $root['position']['board'][$col][$row] = NO_PIECE;
        $col++;
      }
      
      $fen_ptr++;
    }

    if ($col == 8) {
      $col = 0;
      $row--;
      $fen_ptr++;
    }
  }
  
    /* active colour */
  $root['position']['turn'] = ($game_fen[$fen_ptr] == 'w') ? WHITE : BLACK;
  $fen_ptr += 2;

  /* castling availability */
  $root['position']['wkcr'] = $root['position']['wqcr'] = $root['position']['bkcr'] = $root['position']['bqcr'] = FALSE;
  
  while ($game_fen[$fen_ptr] != ' ') {
    $char = $game_fen[$fen_ptr];
    
    switch ($char) {
    case 'K':
      $root['position']['wkcr'] = TRUE;
      break;
    case 'Q':
      $root['position']['wqcr'] = TRUE;
      break;
    case 'k':
      $root['position']['bkcr'] = TRUE;
      break;
    case 'q':
      $root['position']['bqcr'] = TRUE;
      break;
    default:
      break;
    }
    
    $fen_ptr++;
  }
  
  $fen_ptr++;

  /* en passant square */
  $root['position']['ep_col'] = -1;
  if ($game_fen[$fen_ptr] != '-') {
    $root['position']['ep_col'] = ord($game_fen[$fen_ptr]) - ord('a');
    $fen_ptr++;
  }
  
  $fen_ptr += 2;

  sscanf(substr($game_fen, $fen_ptr), "%d, %*d", $root['position']['reversable_moves']);
}

function pgn2html_string_to_move(&$move, $notation) {
  $move['from_col'] = ord($notation[0]) - ord('a');
  $move['from_row'] = $notation[1] - '1';
  $move['to_col'] = ord($notation[2]) - ord('a');
  $move['to_row'] = $notation[3] - '1';

  if ($notation[4] != '\0') {
    $move['promotion_piece'] = pgn2html_piece_to_piece_type(pgn2html_char_to_piece($notation[4]));
  }
  else {
    $move['promotion_piece'] = NO_PIECE_TYPE;
  }
}

/* this function works by fetching a list of legal moves looking for a match */
function pgn2html_algebraic_to_move(&$move, $notation, $position) {
  $piece_type = NO_PIECE_TYPE;
  $pos = 0;
  $move_list = array();

  /* check for castling moves */
  if (!strncmp("O-O-O", $notation, 5)) {
    $move = ($position['turn'] == WHITE) ? pgn2html_wcastlequeenside() : pgn2html_bcastlequeenside();
    return;
  }

  if (!strncmp("O-O", $notation, 3)) {
    $move = ($position['turn'] == WHITE) ? pgn2html_wcastlekingside() : pgn2html_bcastlekingside();
    return;
  }
 
  /* fetch move list */
  // TODO: we dont check on legal moves. It takes to much time (> 30sec)
  // Have to optimize function to do it quicker.
//  pgn2html_get_legal_moves($move_list, $position);
  pgn2html_get_pseudo_legal_moves($move_list, $position);
  $current_move = array_pop($move_list);

  /* identify the type of piece we're looking for */
  $piece_type = pgn2html_char_to_piece_type($notation[0]);
  
  /* loop through the move looking for a match */
  while ($current_move) {
    /* match the piece type */
    if (pgn2html_piece_to_piece_type($position['board'][$current_move['move']['from_col']][$current_move['move']['from_row']]) != $piece_type) {
      $current_move = array_pop($move_list);
      continue;
    }

    $move = pgn2html_null_move();
    
    /* extract co-ordiates and any promotion piece */
    for ($pos = strlen($notation) - 1; $pos >= 0; $pos--) {
        if ($piece_type == PAWN && ctype_upper($notation[$pos])) {
          $move['promotion_piece'] = pgn2html_char_to_piece_type($notation[$pos]);
        }

        if (ctype_lower($notation[$pos]) && $notation[$pos] != 'x') {
          if ($move['to_col'] == -1) {
            $move['to_col'] = ord($notation[$pos]) - ord('a');
          }
          else {
            $move['from_col'] = ord($notation[$pos]) - ord('a');
          }
        }

        if (is_numeric($notation[$pos])) {      
          if ($move['to_row'] == -1) {
            $move['to_row'] = $notation[$pos] - '1';
          }
          else {
            $move['from_row'] = $notation[$pos] - '1';
          }
        }
      }

    /* match co-ordinates */
    if ($move['to_col'] != $current_move['move']['to_col'] || $move['to_row'] != $current_move['move']['to_row']) {
      $current_move = array_pop($move_list);
      continue;
    }

    if (($move['from_col'] != -1 && $move['from_col'] != $current_move['move']['from_col']) ||
        ($move['from_row'] != -1 && $move['from_row'] != $current_move['move']['from_row'])) {
      $current_move = array_pop($move_list);
      continue;
    }

    /* match promotion piece */
    if ($move['promotion_piece'] != $current_move['move']['promotion_piece']) {
      $current_move = array_pop($move_list);
      continue;
    }

    /* we have a match! */
    break;
  }

  /* check if we found a match, if so return it */
  if ($current_move) {
    $move = $current_move['move'];
  }
  else {
    $move['from_col'] = $move['from_row'] = $move['to_col'] = $move['to_row'] = -1;
  }
}

/* get pseudo legal moves and delete those that leave player in check */
function pgn2html_get_legal_moves(&$move_list, $position) {
  pgn2html_get_pseudo_legal_moves($move_list, $position);
  $current = $move_list[0]['id'];
 
  while ($current != -1) {
    $test_position = $position;
    pgn2html_make_move($test_position, $move_list[$current]['move']);
    
    /* if the resultant position is illegal delete the move from the list */
    if (!pgn2html_is_legal_position($test_position)) { 
      $illegal_move = $current;
      if ($move_list[$current]['previous'] > -1) {
        $move_list[$move_list[$current]['previous']]['next'] = $move_list[$current]['next'];
      }
      if ($move_list[$current]['next'] > -1) {
        $move_list[$move_list[$current]['next']]['previous'] = $move_list[$current]['previous'];
      }
      
      $current = $move_list[$current]['next'];
      
      unset($move_list[$illegal_move]);
    }
    else {
      $current = $move_list[$current]['next'];
    }
  }
}

function pgn2html_get_pseudo_legal_moves(&$move_list, $position) {
//  $move_list = pgn2html_new_move_node();
  $move_vector = pgn2html_new_move_vector();
  $from_col = $from_row = $to_col = $to_row = $vector = $range = 0;

  $players_colour = $position['turn'];
  $opponents_colour = ($position['turn'] == WHITE) ? BLACK : WHITE;

  for ($from_col = 0; $from_col < 8; $from_col++) {
    for ($from_row = 0; $from_row < 8; $from_row++) {

      $piece = $position['board'][$from_col][$from_row];

      /* don't bother if it's not a piece of the right colour */
      if (pgn2html_piece_to_colour($piece) != $players_colour) {
        continue;
      }

      /* check type of piece */
      $piece_type = pgn2html_piece_to_piece_type($piece);
      if ($piece_type != PAWN) {

            /* process piece moves */
        for ($vector = 0; $vector < 8; $vector++) {
          pgn2html_move_vector($move_vector, pgn2html_piece_to_piece_type($piece), $vector);
          $to_col = $from_col;
          $to_row = $from_row;

          for ($range = 0; $range < $move_vector['range']; $range++) {
            $to_col += $move_vector['col_vector'];
            $to_row += $move_vector['row_vector'];

            /* if off board stop */
            if ($to_col < 0 || $to_col > 7 || $to_row < 0 || $to_row > 7) {
              break;
            }

            /* if empty square add move to list and continue */
            if ($position['board'][$to_col][$to_row] == NO_PIECE) {
              pgn2html_add_move_to_list($move_list, $from_col, $from_row, $to_col, $to_row);
              continue;
            }

            /* if opponent's piece add move to list and stop */
            if (pgn2html_piece_to_colour($position['board'][$to_col][$to_row]) == $opponents_colour) {
              pgn2html_add_move_to_list($move_list, $from_col, $from_row, $to_col, $to_row);
              break;
            }

            /* if own piece stop */
            if (pgn2html_piece_to_colour($position['board'][$to_col][$to_row]) == $players_colour) {
              break;
            }
          }
        }
      }
      else {
        /* process pawn moves */
        $vector = (pgn2html_piece_to_colour($piece) == WHITE) ? 1 : -1;

        /* single advance? */
        if ($position['board'][$from_col][$from_row + $vector] == NO_PIECE) {

          /* promotion? */
          if (($from_row + $vector) == 0 || ($from_row + $vector) == 7) {
            pgn2html_add_promotions_to_list($move_list, $from_col, $from_row, $from_col, $from_row + $vector);
          }
          else {
            pgn2html_add_move_to_list($move_list, $from_col, $from_row, $from_col, $from_row + $vector);

            /* double advance? */
            if ($from_row == (($position['turn'] == WHITE) ? 1 : 6) && $position['board'][$from_col][$from_row + ($vector * 2)] == NO_PIECE) {
              pgn2html_add_move_to_list($move_list, $from_col, $from_row, $from_col, ($from_row + $vector * 2));
            }
          }
        }

        /* capture left? */
        if ($from_col > 0) {

          /* e.p. capture?*/
          if (($from_col - 1) == $position['ep_col'] && $from_row == (($position['turn'] == WHITE) ? 4 : 3)) {

            /* promotion? */
            if ($from_row + $vector == 0 || $from_row + $vector == 7) {
              pgn2html_add_promotions_to_list($move_list, $from_col, $from_row, $from_col -1, $from_row + $vector);
            }
            else {
              pgn2html_add_move_to_list($move_list, $from_col, $from_row, $from_col - 1, $from_row + $vector);
            }
          }
          else {

            /* normal capture? */
            if (pgn2html_piece_to_colour($position['board'][$from_col - 1][$from_row + $vector]) == $opponents_colour) {

              /* promotion? */
              if ($from_row + $vector == 0 || $from_row + $vector == 7) {
                pgn2html_add_promotions_to_list($move_list, $from_col, $from_row, $from_col - 1, $from_row + $vector);
              }
              else {
                pgn2html_add_move_to_list($move_list, $from_col, $from_row, $from_col - 1, $from_row + $vector);
              }
            }
          }
        }

        /* capture right? */
        if ($from_col < 7) {

          /* e.p. capture?*/
          if (($from_col + 1) == $position['ep_col'] && $from_row == (($position['turn'] == WHITE) ? 4 : 3)) {

            /* promotion? */
            if ($from_row + $vector == 0 || $from_row + $vector == 7) {
              pgn2html_add_promotions_to_list($move_list, $from_col, $from_row, $from_col + 1, $from_row + $vector);
            }
            else {
              pgn2html_add_move_to_list($move_list, $from_col, $from_row, $from_col + 1, $from_row + $vector);
            }
          }
          else {

            /* normal capture? */
            if (pgn2html_piece_to_colour($position['board'][$from_col + 1][$from_row + $vector]) == $opponents_colour) {

              /* promotion? */
              if ($from_row + $vector == 0 || $from_row + $vector == 7) {
                pgn2html_add_promotions_to_list($move_list, $from_col, $from_row, $from_col + 1, $from_row + $vector);
              }
              else {
                pgn2html_add_move_to_list($move_list, $from_col, $from_row, $from_col + 1, $from_row + $vector);
              }
            }
          }
        }
      }

      /* process castling moves */
      if ($piece_type == KING) {
        /* castle kingside? */
        if ((($players_colour == WHITE) ? $position['wkcr'] : $position['bkcr']) &&
           $position['board'][$from_col + 1][$from_row] == NO_PIECE &&
           $position['board'][$from_col + 2][$from_row] == NO_PIECE &&
           !pgn2html_is_square_attacked($position, $from_col, $from_row, $opponents_colour) &&
           !pgn2html_is_square_attacked($position, $from_col + 1, $from_row, $opponents_colour) &&
           !pgn2html_is_square_attacked($position, $from_col + 2, $from_row, $opponents_colour)) {

            pgn2html_add_move_to_list($move_list, $from_col, $from_row, $from_col + 2, $from_row);
        }

        /* castle queenside? */
        if ((($players_colour == WHITE) ? $position['wqcr'] : $position['bqcr']) &&
           $position['board'][$from_col - 1][$from_row] == NO_PIECE &&
           $position['board'][$from_col - 2][$from_row] == NO_PIECE &&
           $position['board'][$from_col - 3][$from_row] == NO_PIECE &&
           !pgn2html_is_square_attacked($position, $from_col, $from_row, $opponents_colour) &&
           !pgn2html_is_square_attacked($position, $from_col - 1, $from_row, $opponents_colour) &&
           !pgn2html_is_square_attacked($position, $from_col - 2, $from_row, $opponents_colour)) {

            pgn2html_add_move_to_list($move_list, $from_col, $from_row, $from_col - 2, $from_row);
        }
      }
    }
  }
}

function pgn2html_is_legal_position($position) {
  $move_list = array();
    
  pgn2html_get_pseudo_legal_moves($move_list, $position);
  $current = $move_list[0]['id'];
  $legal = TRUE;
    
  /* loop through moves checking if a king is captured */
  while ($current > -1) {
    if (pgn2html_piece_to_piece_type($position['board'][$move_list[$current]['move']['to_col']][$move_list[$current]['move']['to_row']]) == KING) {
      $legal = FALSE;
      break;
    }
    
    $current = $move_list[$current]['next'];
  }
  
  return $legal;
}

function pgn2html_is_square_attacked($position, $col, $row, $attacking_colour) {
  $attacked = 0;
  $vector = 0;
  $move_vector = pgn2html_new_move_vector();
  $bishop = pgn2html_piece_type_and_colour_to_piece(BISHOP, $attacking_colour);
  $rook = pgn2html_piece_type_and_colour_to_piece(ROOK, $attacking_colour);
  $queen = pgn2html_piece_type_and_colour_to_piece(QUEEN, $attacking_colour);
  
  /* check for pawn attacks */
  $vector = ($attacking_colour == WHITE) ? -1 : 1;

  if ($col > 0 && $position['board'][$col - 1][$row + $vector] == pgn2html_piece_type_and_colour_to_piece(PAWN, $attacking_colour)) {
    $attacked++;
  }

  if ($col < 7 && $position['board'][$col + 1][$row + $vector] == pgn2html_piece_type_and_colour_to_piece(PAWN, $attacking_colour)) {
    $attacked++;
  }

  /* check for knight attacks */
  for ($vector = 0; $vector < 8; $vector++) {
    pgn2html_move_vector($move_vector, KNIGHT, $vector);
    $attacker_col = $col + $move_vector['col_vector'];
    $attacker_row = $row + $move_vector['row_vector'];
    
    /* make sure square is on board */
    if ($attacker_col >= 0 && $attacker_col < 8 && $attacker_row >= 0 && $attacker_row < 8) {
     
      /* check if square contains any enemy knight */
      if ($position['board'][$attacker_col][$attacker_row] == pgn2html_piece_type_and_colour_to_piece(KNIGHT, $attacking_colour)) {
        $attacked++;
      }
    }
  }

  /* check for attacks from long range pieces */
  $n = $ne = $e = $se = $s = $sw = $w = $nw = TRUE; /* keeps track of which directons are not blocked by a piece yet */

  for ($range = 1; $range <= 7; $range++) {

    /* north */
    if ($n && $row + $range < 8) {
      if ($position['board'][$col][$row + $range] != NO_PIECE) {
        if ($position['board'][$col][$row + $range] == $rook || $position['board'][$col][$row + $range] == $queen) {
          $attacked++;
        }
        else {
          $n = FALSE;
        }
      }
    }
    
    /* north east */
    if ($ne && $col + $range < 8 && $row + $range < 8) {
      if ($position['board'][$col + $range][$row + $range] != NO_PIECE) {
        if ($position['board'][$col + $range][$row + $range] == $bishop || $position['board'][$col + $range][$row + $range] == $queen) {
          $attacked++;
        }
        else {
          $ne = FALSE;
        }
      }
    }

    /* east */
    if ($e && $col + $range < 8) {
      if ($position['board'][$col + $range][$row] != NO_PIECE) {
        if ($position['board'][$col + $range][$row] == $rook || $position['board'][$col + $range][$row] == $queen) {
          $attacked++;
        }
        else {
          $e = FALSE;
        }
      }
    }

    /* south east */
    if ($se && $col + $range < 8 && $row - $range >= 0) {
      if ($position['board'][$col + $range][$row - $range] != NO_PIECE) {
        if ($position['board'][$col + $range][$row - $range] == $bishop || $position['board'][$col + $range][$row - $range] == $queen) {
          $attacked++;
        }
        else {
          $se = FALSE;
        }
      }
    }

  
    /* south */
    if ($s && $row - $range >= 0) {
      if ($position['board'][$col][$row - $range] != NO_PIECE) {
        if ($position['board'][$col][$row - $range] == $rook || $position['board'][$col][$row - $range] == $queen) {
          $attacked++;
        }
        else {
          $s = FALSE;
        }
      }
    }
    
    /* south west */
    if ($sw && $col - $range >= 0 && $row - $range >= 0) {
      if ($position['board'][$col - $range][$row - $range] != NO_PIECE) {
        if ($position['board'][$col - $range][$row - $range] == $bishop || $position['board'][$col - $range][$row - $range] == $queen) {
          $attacked++;
        }
        else {
          $sw = FALSE;
        }
      }
    }

    /* west */
    if ($w && $col - $range >= 0) {
      if ($position['board'][$col - $range][$row] != NO_PIECE) {
        if ($position['board'][$col - $range][$row] == $rook || $position['board'][$col - $range][$row] == $queen) {
          $attacked++;
        }
        else {
          $w = FALSE;
        }
      }
    }

    /* noth west */
    if ($nw && $col - $range >= 0 && $row + $range < 8) {
      if ($position['board'][$col - $range][$row + $range] != NO_PIECE) {
        if ($position['board'][$col - $range][$row + $range] == $bishop || $position['board'][$col - $range][$row + $range] == $queen) {
          $attacked++;
        }
        else {
          $nw = FALSE;
        }
      }
    }
  }

  return $attacked;
}

function pgn2html_make_move(&$position, $move) {
  $piece = $position['board'][$move['from_col']][$move['from_row']];
  $piece_type = pgn2html_piece_to_piece_type($piece);

  if ($piece_type == PAWN) {
    /* if double pawn move, set ep. column */
    if (($move['to_row'] - $move['from_row']) == 2 || ($move['from_row'] - $move['to_row']) == 2) {
      $position['ep_col'] = $move['to_col'];
    }
    else {
      $position['ep_col'] = -1;
    }

    /* if e.p. capture remove captured pawn */
    if ($move['to_col'] != $move['from_col'] && $position['board'][$move['to_col']][$move['to_row']] == NO_PIECE) {
      $position['board'][$move['to_col']][$move['from_row']] = NO_PIECE;
    }
  }

  /* move piece */
  if ($move['promotion_piece'] == NO_PIECE_TYPE) {
    $position['board'][$move['to_col']][$move['to_row']] = $piece;
  }
  else {
    $position['board'][$move['to_col']][$move['to_row']] = pgn2html_piece_type_and_colour_to_piece($move['promotion_piece'], $position['turn']);
  }
  $position['board'][$move['from_col']][$move['from_row']] = NO_PIECE;

  /* castling move corresponding rook also */
  if ($piece_type == KING && (($move['to_col'] - $move['from_col']) == 2 || ($move['from_col'] - $move['to_col'] == 2))) {
    if ($move['to_col'] > $move['from_col']) {
      $position['board'][$move['to_col'] - 1][$move['to_row']] = $position['board'][7][$move['to_row']];
      $position['board'][7][$move['to_row']] = NO_PIECE;
    }
    else {
      $position['board'][$move['to_col'] + 1][$move['to_row']] = $position['board'][0][$move['to_row']];
      $position['board'][0][$move['to_row']] = NO_PIECE;
    }
  }

  /* update castling rights */
  if ($piece_type == KING) {
    if ($position['turn'] == WHITE) {
      $position['wkcr'] = $position['wqcr'] = FALSE;
    }
    else {
      $position['bkcr'] = $position['bqcr'] = FALSE;
    }
  }

  if ($piece_type == ROOK) {
    if ($move['from_row'] == 0) {
      if ($move['from_col'] == 0) {
        $position['wqcr'] = FALSE;
      }
      if ($move['from_col'] == 7) {
        $position['wkcr'] = FALSE;
      }
    }

    if ($move['from_row'] == 7) {
      if ($move['from_col'] == 0) {
        $position['bqcr'] = FALSE;
      }
      if ($move['from_col'] == 7) {
        $position['bkcr'] = FALSE;
      }
    }
  }    

  /* next player's turn */
  $position['turn'] = ($position['turn'] == WHITE) ? BLACK : WHITE;
}

function pgn2html_char_to_piece($char) {
  switch ($char) {
    case 'P':
      return WPAWN;
    case 'N':
      return WKNIGHT;
    case 'B':
      return WBISHOP;
    case 'R':
      return WROOK;
    case 'Q':
      return WQUEEN;
    case 'K':
      return WKING;
    case 'p':
      return BPAWN;
    case 'n':
      return BKNIGHT;
    case 'b':
      return BBISHOP;
    case 'r':
      return BROOK;
    case 'q':
      return BQUEEN;
    case 'k':
      return BKING;
    default:
      return NO_PIECE;
  }
}

function pgn2html_piece_to_char($piece) {
  switch ($piece) {
    case WPAWN:
      return 'P';
    case WKNIGHT:
      return 'N';
    case WBISHOP:
      return 'B';
    case WROOK:
      return 'R';
    case WQUEEN:
      return 'Q';
    case WKING:
      return 'K';
    case BPAWN:
      return 'p';
    case BKNIGHT:
      return 'n';
    case BBISHOP:
      return 'b';
    case BROOK:
      return 'r';
    case BQUEEN:
      return 'q';
    case BKING:
      return 'k';
    default:
      return ' ';
  }
}

function pgn2html_piece_to_piece_type($piece) {
  switch ($piece) {
    case WPAWN:
    case BPAWN:
      return PAWN;
    case WKNIGHT:
    case BKNIGHT:
      return KNIGHT;
    case WBISHOP:
    case BBISHOP:
      return BISHOP;
    case WROOK:
    case BROOK:
      return ROOK;
    case WQUEEN:
    case BQUEEN:
      return QUEEN;
    case WKING:
    case BKING:
      return KING;
    default:
      return NO_PIECE_TYPE;
  }
}

function pgn2html_char_to_piece_type($char) {
  switch ($char) {
    case 'N':
      return KNIGHT;
    case 'B':
      return BISHOP;
    case 'R':
      return ROOK;
    case 'Q':
      return QUEEN;
    case 'K':
      return KING;
    default:
      return PAWN;
  }
}

function pgn2html_piece_type_and_colour_to_piece($piece_type, $colour) {
  return ('$colour' == WHITE) ? $piece_type : ($piece_type + WKING);
}

function pgn2html_piece_to_colour($piece) {
  switch ($piece) {
    case WPAWN:
    case WKNIGHT:
    case WBISHOP:
    case WROOK:
    case WQUEEN:
    case WKING:
      return WHITE;
    case BPAWN:
    case BKNIGHT:
    case BBISHOP:
    case BROOK:
    case BQUEEN:
    case BKING:
      return BLACK;
    default:
      return NO_COLOUR;
  }
}

