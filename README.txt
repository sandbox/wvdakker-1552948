***********
* README: *
***********

DESCRIPTION:
------------
This module provides showing a pgn-file as html.


INSTALLATION:
-------------
1. Place the entire pgn2html directory into your Drupal sites/all/modules/
   directory.

2. Enable the pgn2html module by navigating to:
   administer > modules


Features:
---------
  * shows pgn-files on nodes 
  * adds diagram to game with merida pieces


Note:
-----



Author:
-------
Willem van den Akker
wvdakker@wilsoft.nl