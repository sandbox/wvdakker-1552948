//image data
var pieces = new Array("sq", "wp", "wn", "wb", "wr", "wq", "wk", "bp", "bn", "bb", "br", "bq", "bk");
var variation = 0;
var move = 0;
var flipped = false;
var captured = 0;
var board = new Array();
var piecesDirectory = "";

//Start position, moves and variations
parents = new Array();
moves = new Array();

//initial timerID
var intervalID = -1;

function initialize ()
{
    reset();
    update();
}

function autoplay(interval)
{
  if(intervalID != -1)
  {
    clearInterval(intervalID);
  }

  if(interval != -1)
  {
    intervalID = setInterval("forward()", interval * 1000);
  }
}

function flip()
{
  var temp;
  for(square = 0; square < 32; square++)
  {
    temp = board[square];
    board[square] = board[63 - square];
    board[63 - square] = temp
  }
  flipped = ~flipped;
  update();  
}

function doinit_board ()
{
  var html = "";
  var square = 0;
  var diagramContent = "";

  for (i = 0; i < 8; i++)
  {    
      for (j = 0; j < 8; j++)
        {
          html = "<img class=\"" + colorSq(square) + "\" width=\"40\" height=\"40\" id=s"+ square + ">";
          diagramContent += html;
          square++;
        }
  }
  
  document.getElementById("diagram").innerHTML = diagramContent;
}

function doinit_controls ()
{
  var controlContent = "";

  controlContent += "<p class=played id=\"played\"></p>";
  
  controlContent += "<p>";
  controlContent += "<input type=\"button\" value=\" |&lt; \" onclick=\"first()\">";
  controlContent += "<input type=\"button\" value=\" &lt; \" onclick=\"backward()\">";
  controlContent += "<input type=\"button\" value=\" &gt; \" onclick=\"forward()\">";
  controlContent += "<input type=\"button\" value=\" &gt;| \" onclick=\"last()\">";

  controlContent += "<p>Automatisch:";
  controlContent += "<select style=\"vertical-align: middle\" name=\"autoplay\" onchange=\"autoplay(this.value)\">";
  controlContent += "<option value=\"-1\">Uit";
  controlContent += "<option value=\"1\">1s";
  controlContent += "<option value=\"3\">3s";
  controlContent += "<option value=\"5\">5s";
  controlContent += "<option value=\"10\">10s";
  controlContent += "<option value=\"30\">30s";
  controlContent  += "</select>";
  controlContent += "&nbsp;";
  controlContent += "<input style=\"vertical-align: middle\" type=\"button\" value=\"Keer bord om\" onclick=\"flip()\">";

  controlContent += "<p>";

  document.getElementById("controls").innerHTML = controlContent;
}

function domove()
{
  var source = moves[variation][move * 4];
  var destination = moves[variation][move * 4 + 1];

  if(source < 0)
  {
    return;
  }

  if(flipped)
  {
    source = 63 - source;
    destination = 63 - destination;
  }

  captured = board[destination] != 0; 
  
  board[destination] = board[source];
  board[source] = 0;

  if(moves[variation][move * 4 + 2] < -1)
  {
    board[destination] = - moves[variation][move * 4 + 2];
  }

  source = moves[variation][move * 4 + 2];
  destination = moves[variation][move * 4 + 3];

  if(flipped)
  {
    source = 63 - source;
    destination = 63 - destination;
  }

  if(source >= 0)
  {
    piece = board[source];
    
    board[destination] = board[source];
    board[source] = 0;
  }
      
  move++;
}

function jumpto(target_variation, target_move)
{ 
  var route;
  unhighlight();
  reset();

  while(variation != target_variation)
  {
    route = target_variation;

    while(parents[route][0] != variation)
    {
      route = parents[route][0];
    }

    while(move != parents[route][1])
    {
      domove();
    }

    variation = route;
    move = 0;
  }

  while(move != target_move)
  {
    domove();
  }

  update();
}

function reset()
{
  variation = 0;
  move = 0;

  doinit_board();
  doinit_controls();
 
  for(square = 0; square < 64; square++)
  {
    if(flipped)
    {
      board[square] = initial[63 - square];
    }
    else
    {
      board[square] = initial[square];
    }
  }
}

function unhighlight()
{
  /* reset backgroundcolor from the last move in the list */  
  if(move > 0)
  {
    document.getElementById("v" + variation + "m" + move).style.background = "white";
  }
}

function update()
{
  var html = "&nbsp"; // placeholder
  var zet = 0;
  
  /* set the bitmap for the fields */
  for(square = 0; square < 64; square++)
  {
    document.getElementById("s" + square).src = piece2png(pieces[board[square]]);
  }

  /* set backgroundcolor from the actual move in the list */
  if(move != 0)
  {
      document.getElementById("v" + variation + "m" + move).style.background = "silver";

     if (variation == 0) {
       html = "Stelling na ";
       zet = move;  
      } else {
        html = "Analysediagram na: ";
        zet = move + parents[variation][1];
      }

    if (zet % 2 == 0) 
      // black has played
        html +=  zet / 2 + ". ... "; 
    else
      // white has played
       html += (zet + 1) / 2 + ". ";
       
       html += document.getElementById("v" + variation + "m" + move).textContent;
  }

  document.getElementById("played").innerHTML= html; 
}

function first()
{
  unhighlight();
  reset();
  update();
}

function backward()
{
  if(move > 1)
  {
    jumpto(variation, move - 1);
  }
  else
  {
    if(parents[variation][0] == -1)
    {
      jumpto(0, 0);
    }
    else
    {
      jumpto(parents[variation][0], parents[variation][1]);
    }
  }
}

function forward()
{
  unhighlight();
  domove();
  update();
}

function last()
{
  unhighlight();
  while(moves[variation][move * 4] != -1)
  {
    domove();
  }
  update();
}

/* ------ colorSq ---------- */
function colorSq(sq) {
  if ( (sq%2) == 1 && Math.floor(sq/8) %2 == 0 || (sq%2) == 0 && Math.floor(sq/8) %2 == 1 ) {
    return "blackfield";
  } else { 
    return "whitefield";
  }
}

/* ------  piece2png --------- */
function piece2png(piece) {
    return piecesDirectory+"/"+piece+".png";
}
